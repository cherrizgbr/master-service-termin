package de.cherriz.master.service.termin;

import java.util.List;

/**
 * Schnittstelle zur Verarbeitung von Terminen.
 *
 * @author Frederik Kirsch
 */
public interface TerminService {

    /**
     * Liefert zu einem Vertreter alle zugeordneten Termine.
     *
     * @param id Die VertrterID.
     * @return Die Termine.
     */
    List<Termin> findByVertreter(Long id);

    /**
     * Speichert einen Termin.
     * Gibt den Termin versehen mit ID zurueck.
     *
     * @param termin Der zu speichernde Termin.
     * @return Der gespeicherte Termin.
     */
    Termin create(Termin termin);

}