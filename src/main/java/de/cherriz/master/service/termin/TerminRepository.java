package de.cherriz.master.service.termin;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Schnittstelle zum Repository fuer Termine.
 *
 * @author Frederik Kirsch
 */
public interface TerminRepository extends JpaRepository<Termin, Long> {}