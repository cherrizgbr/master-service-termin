package de.cherriz.master.service.termin;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Die Implementierung des {@link de.cherriz.master.service.termin.TerminService}.
 *
 * @author Frederik Kirsch
 */
@Service
@Validated
public class TerminServiceImpl implements TerminService {

    private final TerminRepository repository;

    @PersistenceContext
    private EntityManager em;

    /**
     * Setzt das Repository fuer den Datenbankzugriff.
     *
     * @param repository Das Repository.
     */
    @Inject
    public TerminServiceImpl(final TerminRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Termin> findByVertreter(Long id) {
        return this.em.createNamedQuery(Termin.TERMIN_FINDBYVERTRETER, Termin.class).setParameter("vertreterID", id).getResultList();
    }

    @Override
    public Termin create(Termin termin) {
        this.repository.save(termin);
        return termin;
    }

}