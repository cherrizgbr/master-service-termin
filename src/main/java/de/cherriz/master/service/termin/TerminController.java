package de.cherriz.master.service.termin;

import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller fuer den REST Service fuer Termine.
 *
 * @author Frederik Kirsch
 */
@RestController
@RequestMapping(value = "/termin")
public class TerminController {

    private final TerminService terminService;

    /**
     * Setzt den Service für die Verarbeitung.
     *
     * @param terminService Der Terminservice.
     */
    @Inject
    public TerminController(final TerminService terminService) {
        this.terminService = terminService;
    }

    /**
     * Methode dient zur Pruefung der Verfuegbarkeit des Services.
     *
     * @return den Status des Services.
     */
    @RequestMapping(value = "/systemcheck", method = RequestMethod.GET)
    public String hello() {
        String result = "SYSTEMCHECK:";
        result += "EntityManager: " + (this.terminService != null ? "OK" : "Error");
        return result;
    }

    /**
     * Liefert zu einem Vertreter die zugeordneten Termine-
     *
     * @param id Die VertreterID.
     * @return Die zugeordneten Termine.
     */
    @RequestMapping(value = "/vertreter/{id}", method = RequestMethod.GET)
    public List<Termin> get(@PathVariable Long id) {
        return this.terminService.findByVertreter(id);
    }

    /**
     * Speichert einen Termin.
     *
     * @param termin Der zu speichernde Termin.
     * @return Der aktualisierte Termin.
     */
    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public Termin create(@RequestBody @Valid Termin termin) {
        return this.terminService.create(termin);
    }

}