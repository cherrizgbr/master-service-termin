package de.cherriz.master.service.termin;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Termin.
 *
 * @author Frederik Kirsch
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = Termin.TERMIN_FINDBYVERTRETER, query = "SELECT t from Termin t where t.vertreterID = :vertreterID")
})
public class Termin {

    /**
     * Named Query zum lesen von Terminen die einem bestimmten Vertreter zugeordnet sind.
     */
    public static final String TERMIN_FINDBYVERTRETER = "Termin.findByVertreter";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long vertreterID;

    private Long kontaktID;

    private String beschreibung;

    private Date beginn;

    private Date ende;

    /**
     * @return Die ID.
     */
    public Long getId() {
        return id;
    }

    /**
     * Setzt die ID.
     * @param id Die ID.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return Die Vertreter ID.
     */
    public Long getVertreterID() {
        return vertreterID;
    }

    /**
     * Setzt den Vertreter.
     *
     * @param vertreterID Der Vertreter.
     */
    public void setVertreterID(Long vertreterID) {
        this.vertreterID = vertreterID;
    }

    /**
     * @return Die KontaktID.
     */
    public Long getKontaktID() {
        return kontaktID;
    }

    /**
     * Setzt den Kontakt.
     *
     * @param kontaktID Der Kontakt.
     */
    public void setKontaktID(Long kontaktID) {
        this.kontaktID = kontaktID;
    }

    /**
     * @return Die Beschreibung.
     */
    public String getBeschreibung() {
        return beschreibung;
    }

    /**
     * Setzt die Terminbeschreibung.
     *
     * @param beschreibung Die Beschreibung.
     */
    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    /**
     * @return Der Terminbeginn.
     */
    public Date getBeginn() {
        return beginn;
    }

    /**
     * Setzt den Beginn des Termins.
     *
     * @param beginn Der Terminbeginn.
     */
    public void setBeginn(Date beginn) {
        this.beginn = beginn;
    }

    /**
     * @return Das Ende des Termins.
     */
    public Date getEnde() {
        return ende;
    }

    /**
     * Setzt das Terminende.
     *
     * @param ende Das Ende des Termins.
     */
    public void setEnde(Date ende) {
        this.ende = ende;
    }

}