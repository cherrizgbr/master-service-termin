INSERT INTO Termin (vertreterID, kontaktID, beschreibung, beginn, ende)
  VALUES (1, 1, "Termin mit Hannes Meyer", NOW()+ INTERVAL 24 HOUR, NOW()+INTERVAL 26 HOUR);
INSERT INTO Termin (vertreterID, kontaktID, beschreibung, beginn, ende)
  VALUES (1, 2, "Termin mit Susanne Kurz", NOW()+ INTERVAL 48 HOUR, NOW()+INTERVAL 50 HOUR);